# Venha trabalhar na ActualSales!

## A Empresa
A ActualSales é uma empresa de marketing digital focada em performance.

## Vagas

### Developer Pleno
Estamos procurando um developer interessado em lidar com variados tipos de sistemas 
e tecnologias.

#### Requisitos:
- Experiência de no mínimo 1 ano em Desenvolvimento Web (PHP) OO
- Conseguir se virar em qualquer situação que surja (no escopo de TI)
- Facilidade para lidar com clientes
- Conhecimento e/ou vontade (principalmente) de aplicar boas práticas e metodologias

#### Bônus:
- Design Patterns, PHPUnit

#### Nosso stack atual:
- PHP (framework baseado no Zend)
- MySQL
- Git (Bitbucket com deploy contínuo)
- jQuery
- Bootstrap
- Apache
- Varnish
- Importante ressaltar que esse stack está sempre aberto para novas tecnologias

#### Projetos:
- Landing Pages
- Plataformas de eCommerce
- CRM
- Aplicativos Mobile e Facebook

#### Oferecemos:
- Ambiente informal
- Comissão Mensal sobre os resultados
- 15 Salários Anuais

#### Como me candidatar?
Não queremos ver o seu CV, mas sim o seu código.
Para isso, elaboramos um pequeno teste de aptidões que pode ser realizado no seu tempo.
As instruções para execução desse teste estão localizadas na pasta landing-page desse mesmo repositório.

#### Dúvidas
Para dúvidas ou mais informações, fique à vontade para nos contactar através do email <marcelo.brito@actualsales.com.br>.


Obrigado e boa sorte!